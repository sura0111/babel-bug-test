module.exports = {
  presets: [
    [
      "next/babel",
      {
        "transform-runtime": {
          useESModules: false,
        },
        "preset-env": {
          debug: true,
          forceAllTransforms: true,
          useBuiltIns: "usage",
          corejs: 3,
          targets: {
            ie: 11,
          },
        },
      },
    ],
  ],

  plugins: [
    "@babel/plugin-transform-arrow-functions",
    "@babel/plugin-transform-for-of",
    "@babel/plugin-syntax-dynamic-import",
    "@babel/plugin-transform-block-scoping",
    [
      "import",
      {
        libraryName: "antd",
        style: true,
      },
    ],
    [
      "module-resolver",
      {
        root: ["./src"],
      },
    ],
  ],
};
