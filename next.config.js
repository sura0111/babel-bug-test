const path = require("path");
const withLess = require("@zeit/next-less");
const withCSS = require("@zeit/next-css");
const withTM = require("next-transpile-modules")(
  [
    "@ant-design",
    "@antv",
    "react-content-loader",
    "@ampproject/toolbox-optimizer",
    "@ampproject/toolbox-runtime-version",
    "@ampproject/toolbox-script-csp",
    "@ampproject/toolbox-validator-rules",
    "@hapi/accept",
    "@hapi/boom",
    "@hapi/hoek",
    "@next/env",
    "@npmcli/move-file",
    "@zeit/next-css",
    "@zeit/next-less",
    "abort-controller",
    "agent-base",
    "aggregate-error",
    "ansi-styles",
    "anymatch",
    "asn1.js",
    "bl",
    "braces",
    "cacache",
    "caller-callsite",
    "caller-path",
    "callsites",
    "camelcase",
    "chalk",
    "chokidar",
    "chownr",
    "clean-stack",
    "colorette",
    "constants-browserify",
    "core-js-compat",
    "cosmiconfig",
    "cssnano-preset-simple",
    "cssnano-simple",
    "d3-dispatch",
    "d3-force",
    "d3-hierarchy",
    "d3-quadtree",
    "d3-timer",
    "data-uri-to-buffer",
    "decompress-response",
    "enhanced-resolve",
    "escalade",
    "eslint-scope",
    "event-target-shim",
    "figgy-pudding",
    "fill-range",
    "find-cache-dir",
    "find-up",
    "fs-minipass",
    "fsevents",
    "has-flag",
    "https-proxy-agent",
    "import-cwd",
    "import-fresh",
    "import-from",
    "indent-string",
    "infer-owner",
    "is-any-array",
    "is-binary-path",
    "is-wsl",
    "jest-worker",
    "jsesc",
    "json-parse-better-errors",
    "leven",
    "loader-utils",
    "locate-path",
    "lru-cache",
    "make-dir",
    "merge-stream",
    "mimic-response",
    "minipass",
    "minipass-collect",
    "minipass-flush",
    "minipass-pipeline",
    "minizlib",
    "ml-array-max",
    "ml-array-min",
    "ml-array-rescale",
    "ml-matrix",
    "nanoid",
    "native-request",
    "next",
    "next-transpile-modules",
    "node-addon-api",
    "node-fetch",
    "normalize-html-whitespace",
    "p-limit",
    "p-locate",
    "p-map",
    "p-try",
    "parse-json",
    "path-exists",
    "pdfjs-dist",
    "pify",
    "pkg-dir",
    "pkg-up",
    "postcss-load-config",
    "postcss-modules-extract-imports",
    "promise-inflight",
    "readdirp",
    "regexpu-core",
    "resolve-from",
    "schema-utils",
    "sharp",
    "simple-get",
    "slash",
    "ssri",
    "supports-color",
    "terser",
    "to-fast-properties",
    "to-regex-range",
    "tr46",
    "ts-pnp",
    "unicode-match-property-ecmascript",
    "unicode-match-property-value-ecmascript",
    "watchpack",
    "webidl-conversions",
    "which-pm-runs",
    "worker-farm",
    "yallist",
  ],
  {
    resolveSymlinks: true,
  }
);

if (typeof require !== "undefined") {
  require.extensions[".less"] = () => {};
}

module.exports = withCSS(
  withLess(
    withTM({
      lessLoaderOptions: {
        lessOptions: {
          javascriptEnabled: true,
        },
      },
      webpack: (config, { dev, isServer }) => {
        if (isServer) {
          const antStyles = /antd\/.*?\/style.*?/;
          const origExternals = [...config.externals];
          config.externals = [
            (context, request, callback) => {
              if (request.match(antStyles)) return callback();
              if (typeof origExternals[0] === "function") {
                origExternals[0](context, request, callback);
              } else {
                callback();
              }
            },
            ...(typeof origExternals[0] === "function" ? [] : origExternals),
          ];
          config.module.rules.unshift({
            test: antStyles,
            use: "null-loader",
          });
        }

        const originalEntry = config.entry;
        config.entry = async () => {
          const entries = await originalEntry();
          if (entries["main.js"]) {
            entries["main.js"].unshift("./client/polyfills.js"); // <- polyfill here
          }
          return entries;
        };

        // config.resolve.alias = Object.assign({}, config.resolve.alias, {
        //   "@ant-design/charts": "@ant-design/charts/lib/index.js",
        // });

        // config.module.rules.push({
        //   test: /\.jsx$/,
        //   use: {
        //     loader: "babel-loader",
        //     options: {
        //       presets: ["@babel/preset-env"],
        //     },
        //   },
        // });
        // This lib has an arrow fn causing IE11 to explode
        // config.module.rules.push({
        //   test: /\..*?js$/,
        //   include: {
        //     test: /node_modules\/@ant-design\/charts/, // Exclude libraries in node_modules ...
        //   },
        //   use: {
        //     loader: "babel-loader",
        //     options: {
        //       babelrc: false,
        //       cacheDirectory: true,
        //       presets: [
        //         [
        //           "next/babel",
        //           {
        //             "transform-runtime": {
        //               useESModules: false,
        //             },
        //             "preset-env": {
        //               debug: true,
        //               forceAllTransforms: true,
        //               useBuiltIns: "usage",
        //               corejs: 3,
        //               targets: {
        //                 ie: 11,
        //               },
        //             },
        //           },
        //         ],
        //       ],
        //       plugins: [
        //         require("@babel/plugin-transform-for-of"),
        //         require("@babel/plugin-transform-arrow-functions"),
        //       ],
        //     },
        //   },
        // });
        console.log("this webpack");
        return config;
      },
    })
  )
);
