import React, { useState, useEffect } from "react";
import Router from "next/router";
import { Button, Form, Input } from "antd";

import dynamic from "next/dynamic";
// const antChartPie = dynamic(() => import("@ant-design/charts/lib/pie/index"), {
//   ssr: false,
// });
// console.log(antChartPie);

const one = () => {
  const [dummy, setDummy] = useState(0);

  const [antChart, setAntChart] = useState(null);
  // console.log(antChartPie);
  useEffect(() => {
    const loadAntChart = async () => {
      if (process.browser) {
        window.addEventListener("unhandledrejection", (event) => {
          console.warn(`UNHANDLED PROMISE REJECTION: ${event.reason}`);
        });
        const antc = (await import("@ant-design/charts")).default;
        console.dir(antc);
        console.log("antc", antc);
        setAntChart(antc);
      }
    };
    loadAntChart();
  }, []);
  const data = [
    {
      type: "Classification One",
      value: 27,
    },
    {
      type: "Class 2",
      value: 25,
    },
    {
      type: "Classification Three",
      value: 18,
    },
    {
      type: "Classification Four",
      value: 15,
    },
    {
      type: "Classification Five",
      value: 10,
    },
    {
      type: "Other",
      value: 5,
    },
  ];

  const config = {
    appendPadding: 10,
    data,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "outer",
      content: "{name} {percentage}",
    },
    interactions: [{ type: "pie-legend-active" }, { type: "element-active" }],
  };

  return (
    <div>
      {/* {antChart && process.browser && <antChart {...config} />} */}
      {/* {antChartPie && process.browser ? "true" : "false"} */}
      <Button
        onClick={() => {
          setDummy(dummy + 1);
          console.log("antChart", antChart);
          // alert(JSON.stringify(antCharta, null, 2));
        }}
      >
        Click
      </Button>
      {dummy}
      <Form onFinish={() => Router.push("/two")}>
        <Form.Item name="name">
          <Input />
        </Form.Item>
        <Button htmlType="submit">Hey</Button>
      </Form>
    </div>
  );
};

export default one;
